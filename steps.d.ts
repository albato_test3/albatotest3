/// <reference types='codeceptjs' />
type emailGeneration = typeof import('./modules/email_generation.js');
type loginPage = typeof import('./pages/login.js');
type bundlePage = typeof import('./pages/bundle.js');
type bundleConstructorPage = typeof import('./pages/bundle_constructor.js');
type passwordResetPage = typeof import('./pages/password_reset.js');
type signupPage = typeof import('./pages/signup.js');
type solutionsMarketplacePage = typeof import('./pages/solutions_marketplace.js');
type solutionsWizardPage = typeof import('./pages/solutions_wizard.js');
type ChaiWrapper = import('codeceptjs-chai');
type MailSlurp = import('@codeceptjs/mailslurp-helper');

declare namespace CodeceptJS {
  interface SupportObject { I: I, current: any, emailGeneration: emailGeneration, loginPage: loginPage, bundlePage: bundlePage, bundleConstructorPage: bundleConstructorPage, passwordResetPage: passwordResetPage, signupPage: signupPage, solutionsMarketplacePage: solutionsMarketplacePage, solutionsWizardPage: solutionsWizardPage }
  interface Methods extends Playwright, REST, JSONResponse, ChaiWrapper, MailSlurp {}
  interface I extends WithTranslation<Methods> {}
  namespace Translation {
    interface Actions {}
  }
}
