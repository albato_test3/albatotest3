const { I } = inject();

//Получение адреса временной электронной почты
async function getMailbox () {
    const mailbox = await I.haveNewMailbox();
    return mailbox.emailAddress;
};

//Получение кода подтверждения почты.
async function getCodeEmail () {
    const message = await I.waitForLatestEmail(30); //Метод 30 секунд ожидает получения письма. Возвращает код из последнего письма последнего созданного почтового ящика.
    
    //Достаем из тела ответа код подтверждения почты
    const start = 'strong>'; //Строка, после которой находится код в письме
    const end = '&nbsp'; //Строка, перед которой находится код в письме
    const regex = new RegExp(`${start}(.*?)${end}`); //Поиск кода в письме
    const code = message.body.match(regex)[1]; //Присваиваем найденный код в переменную
    return code
};

//Получение кода подтверждения почты после повторной отправки
async function getCodeEmailResend () {
    const message = await I.waitForLatestEmail(30); //Метод 30 секунд ожидает получения письма. Возвращает код из последнего письма последнего созданного почтового ящика.
    
    //Достаем из тела ответа код подтверждения почты
    const start = 'strong>'; //Строка, после которой находится код в письме
    const end = '</strong'; //Строка, перед которой находится код в письме
    const regex = new RegExp(`${start}(.*?)${end}`); //Поиск кода в письме
    const code = message.body.match(regex)[1]; //Присваиваем найденный код в переменную
    return code
};

//Получение ссылки для сброса пароля
async function getLinkPasswordReset () {
    const message = await I.waitForLatestEmail(30); //Метод 30 секунд ожидает получения письма. Возвращает код из последнего письма последнего созданного почтового ящика.
    
    //Достаем из тела ответа ссылку для сброса пароля
    const start = 'href=\\"'; //Строка, после которой находится ссылка в письме
    const end = '\\" style='; //Строка, перед которой находится ссылка в письме
    const contains = 'reset-password'; //Строка, которую содержит ссылка в письме
    const regex = new RegExp(`${start}(.*?${contains}.*?)${end}`); //Поиск ссылки в письме
    const link = message.body.match(regex)[1]; //Присваиваем найденную ссылку в переменную
    return link
};

module.exports = {
    getMailbox,
    getCodeEmail,
    getCodeEmailResend,
    getLinkPasswordReset
}