const { I } = inject();

//Отправление письма со ссылкой для изменения пароля на указанный в аргументе функции аккаунт
async function restorePassword (mail) {
    await I.sendPostRequest('https://api.albato-staging.com/user/auth/restore-password', {
        "email": mail});
};

module.exports = {
    restorePassword
}