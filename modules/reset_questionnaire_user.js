const { I } = inject();

//Сброс признака заполнения анкеты "О себе"
async function resetQuestionnaireUser (email, password) {

    //Получаем токен авторизации
    const token = await I.sendPostRequest('https://api.albato-staging.com/user/auth', {
        "email": email,
        "password": password});
    
    //Сбрасываем анкету
    await I.sendPostRequest('https://api.albato-staging.com/user/profile', {
        "questionnaireStatus": 0
    },
    {
        "Authorization": `Bearer ${token.data.data.authToken}`
    });
};

module.exports = {
    resetQuestionnaireUser
}