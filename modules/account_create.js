const { I, emailGeneration } = inject();

//Регистрация подтвержденного аккаунта в Albato
async function accountReg () {
    let mail = await emailGeneration.getMailbox();
    let password = '123tests';
    let name = `autotest${Date.now()}`;
    const response = await I.sendPostRequest('https://api.albato-staging.com/user/auth/sign-up', {
        "name": name,
        "country": "ru",
        "phone": "79998887766",
        "email": mail,
        "password": password,
        "language": 'ru',
        "questionnaireStatus": 2
    });
    const code = await emailGeneration.getCodeEmail();
    await I.sendPostRequest('https://api.albato-staging.com/user/email-confirmation', {
        "code": code,
        "userId": response.data.data.id});
    return {mail, password, name};
};

//Регистрация неподтвержденного аккаунта в Albato
async function accountRegNotConfirmed () {
    let mail = await emailGeneration.getMailbox();
    let password = '123tests';
    let name = `autotest${Date.now()}`;
    await I.sendPostRequest('https://api.albato-staging.com/user/auth/sign-up', {
        "name": name,
        "country": "ru",
        "phone": "79998887766",
        "email": mail,
        "password": password,
        "language": 'ru',
        "questionnaireStatus": 2
    });
    return {mail, password, name};
};

module.exports = {
    accountReg,
    accountRegNotConfirmed
}