const { I } = inject();

class BundleConstructorPage {

    //Локаторы
   
    urlBundleConstructor = '/bundle/edit/'; //URL страницы
    fieldHeader = {
        header: '.al-header', //Главный хедер
        menu: {
            dropDown: '.al-user-menu__control', //Кнопка выпадающего меню
            profile: '//li[1]', //Кнопка страницы профиля
            builderApp: '//li[2]', //Кнопка страницы конструктора приложений
            builderSolutions: '//li[3]', //Кнопка страницы конструктора решений
            oldAlbato: '//li[4]', //Кнопка страницы старого интерфейса
            signOut: '//li[5]' //Кнопка выхода из системы
        }
    };
    fieldStepsBundle = '.al-bundle-editor__container'; //Поле с шагами связки

    //Методы
    
    //Открытие страницы конструктора связки. Обязателен параметр с ID связки
    openPage (params = '') {
    I.amOnPage(this.urlBundleConstructor + params);
    };
    
    //Проверка текущего адреса страницы
    async seeCurrentPage (params = '') {
        I.assertContain(await I.grabCurrentUrl(), this.urlBundleConstructor + params);
    };

    //Ожидание загрузки главного хедера
    waitHeader () {
        I.waitForElement(this.fieldHeader.header, 10);
    };

    //Ожидание загрузки поля с конструктором связки
    waitStepsBundle () {
        I.waitForElement(this.fieldStepsBundle, 10);
    };

    //Наличие указанного текста в поле с шагами связки
    seeTextInStepsBundle (value) {
        I.see(value, this.fieldStepsBundle);
    }
};

module.exports = new BundleConstructorPage();