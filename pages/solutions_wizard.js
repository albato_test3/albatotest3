const { I } = inject();

class SolutionsWizard {

    //Локаторы

    urlSolutionsWizard = '/solutions/wizard/'; //URL страницы
    fieldHeader = {
        header: '.al-header', //Главный хедер
        menu: {
            dropDown: '.al-user-menu__control', //Кнопка выпадающего меню
            profile: '//li[1]', //Кнопка страницы профиля
            builderApp: '//li[2]', //Кнопка страницы конструктора приложений
            builderSolutions: '//li[3]', //Кнопка страницы конструктора решений
            oldAlbato: '//li[4]', //Кнопка страницы старого интерфейса
            signOut: '//li[5]' //Кнопка выхода из системы
        }
    };
    fieldScenarios = '//div[2]/div[2]/div'; //Поле со сценариями решения
    fieldNameSolution = '.mb-8'; //Поле c названием решения

    //Методы

    //Открытие страницы установки решения. Обязателен параметр с ID решения для установки
    openPage (params = '') {
        I.amOnPage(this.SolutionsWizard + params);
    };
    
    //Проверка текущего адреса страницы
    async seeCurrentPage (params = '') {
        I.assertContain(await I.grabCurrentUrl(), this.urlSolutionsWizard + params);
    };

    //Ожидание загрузки главного хедера
    waitHeader () {
        I.waitForElement(this.fieldHeader.header, 10);
    };

    //Ожидание загрузки поля со сценариями решения
    waitScenarios () {
        I.waitForElement(this.fieldScenarios, 10);
    };

    //Наличие указанного названия решения
    seeNameSolution (value) {
        I.see(value, this.fieldNameSolution);
    };
};

module.exports = new SolutionsWizard();