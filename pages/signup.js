const { I } = inject();

class SignupPage {
    
//Cтраница регистрации
    urlSignup = '/user/signup'; //URL страницы
    formSignup = '.al-register-form'; //Форма регистрации
    logoAlbato = '.al-auth-modal-logo'; //Логотип Albato
    buttonLanguage = '//form/div[1]/div/div'; //Поле для выбора языка
    fieldTitleForm = '.al-auth-form-wrapper__titleh'; //Поле с названием формы
    textName = '//div[2]/div/input'; //Поле для ввода имени
    buttonCountry = '//form/div[3]/div/div[1]'; //Поле выбора страны
    fieldLoaderCountry = '.al-spinner'; //Поле с индикатором ожидания идентификации страны пользователя
    textSearchCountry = '.al-select__search-input'; //Поле для поиска значений выпадающего списка стран
    buttonClearCountry = '.icon-close'; //Кнопка очистки поля поиска значений выпадающего списка стран
    buttonFlagPhone = '.selected-flag'; //Поле выбора страны телефона
    textPhone = '.al-phone-input__element' //Поле для ввода телефона
    textEmail = '//div[5]/div/input'; //Поле для ввода почты
    textPassword = '//div[6]/div/input'; //Поле для ввода пароля
    buttonHidePassword = '.al-icon-button_height_available'; //Кнопка видимости пароля
    butonConditions = '#sign-up-agreement'; //Кнопка принятия условий использования
    buttonAgreement = '//div[2]/div/a[1]'; //Кнопка страницы пользовательского соглашения
    buttonPolitics = '//div[2]/div/a[2]'; //Кнопка страницы политики конфиденциальности
    buttonSignup = '.al-button_variant_contained'; //Кнопка регистрации
    pagePasswordReset = '//div[10]/div/a[1]'; //Кнопка страницы восстановления пароля
    pageLogin = '//div[10]/div/a[2]'; //Кнопка страницы авторизации
    fieldSnackbar = '.al-snackbar'; //Поле снэкбара
    buttonFacebook = '//div[2]/button[1]'; //Кнопка регистрации через Facebook
    buttonGoogle = '//div[2]/button[2]'; //Кнопка регистрации через Google
    
//Вкладка авторизации в Facebook 
    tabFacebook = {
        formLoginFacebook: '#loginform', //Форма авторизации в Facebook
        textEmailFacebook: '[name="email"]', //Поле для ввода почты в Facebook
        textPasswordFacebook: '[name="pass"]', //Поле для ввода пароля в Facebook
        buttonLoginFacebook: '[name="login"]' //Кнопка авторизации в Facebook
    };
    
//Страница анкеты о пользователе
    questionnaireUser = {
        modal: '.al-modal', //Модальное окно анкеты
        fieldTitleModal: '.al-modal__header', //Поле с названием модального окна
        buttonQuestion1: '//label[1]/div[2]', //Поле для выбора ответа на 1 вопрос
        buttonQuestion2: '//label[2]/div[2]', //Поле для выбора ответа на 2 вопрос
        buttonQuestion3: '//label[3]/div[2]', //Поле для выбора ответа на 3 вопрос
        buttonQuestion4: '//label[4]/div[2]', //Поле для выбора ответа на 4 вопрос
        buttonQuestion5: '//label[5]/div[2]', //Поле для выбора ответа на 5 вопрос
        textSearch: '.al-select__search-input', //Поле для поиска значений выпадающего списка
        buttonClear: '.icon-close', //Кнопка очистки поля поиска значений выпадающего списка
        buttonSaveUser: '//button', //Кнопка сохранения анкеты
        buttonSkip: '//div[3]/button/div' //Кнопка пропуска анкеты
    };

//Страница анкеты о сервисах
    questionnaireServices = {
        modal: '.al-modal', //Модальное окно анкеты
        fieldTitleModal: '.al-modal__header', //Поле с названием модального окна
        textSearch: '.al-input__element', //Поле для поиска сервисов по названию
        fieldTags: '//div[3]/div[3]/div/div/div', //Поле с тегами выбранных сервисов
        fieldCounter: '//div[3]/div[4]', //Поле с количеством выбранных сервисов
        buttonSkip: '//div[4]/button[1]', //Кнопка пропуска анкеты
        buttonSaveServices: '//div[4]/button[2]', //Кнопка сохранения анкеты
        fieldServicesList: '//*[@id="infiniteScrollContainer"]/div/div' //Поле со списком сервисов
    };

//Страница подтверждения почты по коду
    emailConfirm = {
        formConfirm: '.al-register-form', //Форма подтверждения почты
        logoAlbato: '.al-auth-modal-logo', //Логотип Albato
        buttonLanguage: '//form/div[1]/div/div', //Поле для выбора языка
        fieldTitleForm: '.al-auth-form-wrapper__titleh', //Поле с названием формы
        fieldEmail: '//b', //Поле с указанным при регистрации адресом почты
        textCode: '.al-input__element', //Поле для ввода кода подтверждения почты
        buttonResendCode: '//button', //Кнопка переотправки кода подтверждения почты
        fieldTimer: '.ml-1', //Поле с таймером
        fieldErrors: '.al-input__error' ////Поле c ошибками подтверждения почты
    };

//Страница приветствия
    welcomePage = {
        urlWelcome: '/welcome', //URL страницы
        fieldTitlePage: '//h2', //Поле с текстом приветствия
        buttonCreateBundle: '.al-button' //Кнопка создания связки 
    };

//Методы для страницы регистрации

    //Открытие страницы регистрации. Можно указать парметры c '&'
    openPage (params = '') {
        I.amOnPage(this.urlSignup + '?lang=ru' + params);
    };

    //Проверка текущего адреса страницы
    async seeCurrentPage (params = '') {
        I.assertContain(await I.grabCurrentUrl(), this.urlSignup + params);
    };

    //Ожидание загрузки формы регистрации
    waitFormSignup () {
        I.waitForElement(this.formSignup, 10);
    };

    //Клик по логотипу Albato
    clickLogo () {
        I.click(this.logoAlbato);
    };

    //Выбор указанного языка
    selectLanguage (value) {
        I.click(this.buttonLanguage);
        I.click(`//*[text()="${value}"]`, '//ul');
    };

    //Наличие указанного названия формы. Для русского языка интерфейса название выставлено по умолчанию
    seeTitleForm (value = 'Зарегистрируйте аккаунт') {
        I.seeTextEquals(value, this.fieldTitleForm);
    };

    //Ввод имени
    enterName (value) {
        I.fillField(this.textName, value);
    };

    //Выбор страны из выпадающего списка
    selectCountry (value) {
        I.waitToHide(this.fieldLoaderCountry, 10);
        I.click(this.buttonCountry);
        I.click(`//*[text()="${value}"]`, '//ul');
    };

    //Ввод текста в выпадающем списке стран для поиска страны по названию
    enterCountry (value) {
        I.waitToHide(this.fieldLoaderCountry, 10);
        I.click(this.buttonCountry);
        I.fillField(this.textSearchCountry, value);
    };

    //Клик по кнопке очистки текста в выпадающем списке стран
    clickClearCountry () {
        I.click(this.buttonClearCountry);
    };

    //Наличие указанного значения при поиске страны по названию
    seeValueInCountryList (value) {
        I.seeTextEquals(value, '//li[1]');
    };

    //Выбор страны телефона
    selectFlagPhone (value) {
        I.waitToHide(this.fieldLoaderCountry, 10);
        I.click(this.buttonFlagPhone);
        I.click(`//*[text()="${value}"]`, );
    };

    //Наличие указанного значения в поле телефона
    seeValueInPhone (value) {
        I.seeInField(this.textPhone, value);
    };

    //Ввод телефона
    enterPhone (value) {
        I.fillField(this.textPhone, value);
    };

    //Ввод почты
    enterEmail (value) {
        I.fillField(this.textEmail, value);
    };

    //Ввод пароля
    enterPassword (value) {
        I.fillField(this.textPassword, value);
    };

    //Наличие указанного значения в поле пароля
    seeValueInPassword (value) {
        I.seeInField(this.textPassword, value);
    };

    //Клик по кнопке видимости пароля
    clickHidePassword () {
        I.click(this.buttonHidePassword);
    };

    //Клик по кнопке принятия условий использования
    clickConditions () {
        I.forceClick(this.butonConditions);
    };

    //Клик по кнопке страницы пользовательского соглашения
    clickAgreement () {
        I.click(this.buttonAgreement);
    };

    //Клик по кнопке страницы политики конфиденциальности
    clickPolitics () {
        I.click(this.buttonPolitics);
    };

    //Клик по кнопке регистрации
    clickSignup () {
        I.click(this.buttonSignup);
    };

    //Клик по кнопке страницы восстановления пароля
    clickPagePasswordReset () {
        I.click(this.pagePasswordReset);
    };

    //Клик по кнопке страницы авторизации
    clickPageLogin () {
        I.click(this.pageLogin);
    };

    //Ожидание загрузки снэкбара
    waitSnackbar () {
        I.waitForElement(this.fieldSnackbar, 10);
    };

    //Наличие указанного сообщения внутри снэкбара
    seeSnackbar (value) {
        I.seeTextEquals(value, this.fieldSnackbar);
    };

    //Клик по кнопке регистрации через Facebook
    clickFacebook () {
        I.wait(5); //Клик без задержки не срабатывает
        I.click(this.buttonFacebook);
        I.wait(5); //Время между кликом и появлением вкладки Facebook
    };

    //Клик по кнопке регистрации через Google
    clickGoogle () {
        I.wait(5); //Клик без задержки не срабатывает
        I.click(this.buttonGoogle);
        I.wait(5); //Время между кликом и появлением вкладки Google
    };

    //Переключение на следующую вкладку (по умолчанию), либо на указанную
    switchTab (value) {
        I.switchToNextTab(value);
    };

//Методы для вкладки авторизации в Facebook

    //Заполнение формы авторизации в Facebook
    authFacebook (email, password) {
        I.waitForElement(this.tabFacebook.formLoginFacebook, 10);
        I.fillField(this.tabFacebook.textEmailFacebook, email);
        I.fillField(this.tabFacebook.textPasswordFacebook, password);
        I.forceClick(this.tabFacebook.buttonLoginFacebook);
        I.wait(10); //Без ожидания тесты будут падать
    };

//Методы для страницы анкеты о пользователе

    //Ожидание загрузки модального окна анкеты о пользователе
    waitModalQuestionnaireUser () {
        I.waitForElement(this.questionnaireUser.modal, 10);
    };

    //Наличие указанного названия модального окна анкеты о пользователе. Для русского языка интерфейса название выставлено по умолчанию
    seeTitleQuestionnaireUser (value = 'Помогите нам стать лучше') {
        I.waitForText('', 10, this.questionnaireUser.fieldTitleModal);
        I.seeTextEquals(value, this.questionnaireUser.fieldTitleModal);
    };

    //Заполнение анкеты о пользователе. По умолчанию выбираются первые значения для всех выпадающих списков, либо указанные в аргументе метода.
    fillQuestionnaireUser (value = '1') {
        I.click(this.questionnaireUser.buttonQuestion1);
        I.click(`//li[${value}]`, '//ul');
        I.click(this.questionnaireUser.buttonQuestion2);
        I.click(`//li[${value}]`, '//ul');
        I.click(this.questionnaireUser.buttonQuestion3);
        I.click(`//li[${value}]`, '//ul');
        I.click(this.questionnaireUser.buttonQuestion4);
        I.click(`//li[${value}]`, '//ul');
        I.click(this.questionnaireUser.buttonQuestion5);
        I.click(`//li[${value}]`, '//ul');
    };

    //Ввод текста в выпадающем списке анкеты о пользователе. В методе используется первый вопрос анкеты.
    enterAnswer (value) {
        I.click(this.questionnaireUser.buttonQuestion1);
        I.fillField(this.questionnaireUser.textSearch, value);
    };

    //Клик по кнопке очистки текста в выпадающем списке анкеты о пользователе
    clickClearAnswer () {
        I.click(this.questionnaireUser.buttonClear);
    };

    //Наличие указанного значения в выпадающем списке анкеты о пользователе
    seeValueInAnswerList (value) {
        I.seeTextEquals(value, '//li');
    };

    //Клик по кнопке сохранения анкеты о пользователе
    clickSaveUser () {
        I.click(this.questionnaireUser.buttonSaveUser)
    };

    //Наличие признака блокировки у кнопки сохранения анкеты о пользователе
	async seeDisabledSaveUser () {
        I.assertEqual(await I.grabAttributeFrom(this.questionnaireUser.buttonSaveUser, 'disabled'), true)
    };

    //Клик по кнопке пропуска анкеты
    clickSkipUser () {
        I.click(this.questionnaireUser.buttonSkip);
    };

//Методы для страницы анкеты о сервисах

    //Ожидание загрузки модального окна анкеты о сервисах
    waitModalQuestionnaireServices () {
        I.waitForElement(this.questionnaireServices.modal, 10);
    };

    //Наличие указанного названия модального окна анкеты о сервисах. Для русского языка интерфейса название выставлено по умолчанию
    seeTitleQuestionnaireServices (value = 'Какими сервисами вы пользуетесь?') {
        I.waitForText('', 10, this.questionnaireServices.fieldTitleModal);
        I.seeTextEquals(value, this.questionnaireServices.fieldTitleModal);
    };

    //Ввод названия сервисов в анкете о сервисах
    enterServices (value) {
        I.fillField(this.questionnaireServices.textSearch, value);
    };

    //Удаление названия сервисов в анкете о сервисах
    clearServices () {
        I.pressKey(['CommandOrControl', 'Backspace'])
    };

    //Клик по указанному количеству тегов с выбранными сервисами
    clickTags (value) {
        let i = 1;
        while (i <= value) {
            I.click(this.questionnaireServices.fieldTags + `/div[${i}]`);
            i++;
        };
    };

    //Наличие указанного значения в поле с количеством выбранных сервисов
    seeCounter (value) {
        I.see(value, this.questionnaireServices.fieldCounter);
    };

    //Клик по кнопке пропуска анкеты
    clickSkipServices () {
        I.click(this.questionnaireServices.buttonSkip);
    };

    //Клик по кнопке сохранения анкеты
    clickSaveServices () {
        I.click(this.questionnaireServices.buttonSaveServices);
    };

    //Ожидание загрузки указанного сервиса в списке сервисов
    waitServices (value) {
        I.waitForText(value, 10, this.questionnaireServices.fieldServicesList);
    };

    //Выбор указанного количества сервисов в списке сервисов
    selectServices (value) {
        let i = 1;
        while (i <= value) {
            I.click(this.questionnaireServices.fieldServicesList + `/div[${i}]`);
            i++;
        };
    };

    //Прокрутка списка сервисов до указанного по счету сервиса в списке
    scrollServices (value) {
        I.click(this.questionnaireServices.fieldServicesList + `/div[${value}]`);
    };

    //Наличие признака блокировки у кнопки сохранения анкеты о сервисах
	async seeDisabledSaveServices () {
        I.assertEqual(await I.grabAttributeFrom(this.questionnaireServices.buttonSaveServices, 'disabled'), true)
    };

    //Наличие признака блокировки у кнопки пропуска анкеты о сервисах
	async seeDisabledSkip () {
        I.assertEqual(await I.grabAttributeFrom(this.questionnaireServices.buttonSkip, 'disabled'), true)
    };

//Методы для страницы подтверждения почты по коду

    //Ожидание загрузки формы подтверждения почты
    waitFormConfirm () {
        I.waitForElement(this.emailConfirm.formConfirm, 10);
    };

    //Клик по логотипу Albato
    clickLogoConfirm () {
        I.click(this.emailConfirm.logoAlbato);
    };

    //Выбор указанного языка
    selectLanguageConfirm (value) {
        I.click(this.emailConfirm.buttonLanguage);
        I.click(`//*[text()="${value}"]`, '//ul');
    };

    //Наличие указанного названия формы. Для русского языка интерфейса название выставлено по умолчанию
    seeTitleFormConfirm (value = 'Проверьте свой email') {
        I.seeTextEquals(value, this.emailConfirm.fieldTitleForm);
    };

    //Наличие указанного при регистрации адреса почты
    seeEmailUser (value) {
        I.seeTextEquals(value, this.emailConfirm.fieldEmail);
    };

    //Ввод кода подтверждения почты
    enterCode (value) {
        I.fillField(this.emailConfirm.textCode, value);
    };

    //Клик по кнопке переотправки кода подтверждения почты
    clickResend () {
        I.click(this.emailConfirm.buttonResendCode);
    };

    //Ожидание истечения таймера
    waitTimerExpired () {
        I.waitToHide(this.emailConfirm.fieldTimer, 70);
    };

    //Ожидание загрузки поля с ошибками подтверждения почты
	waitErrorsConfirm () {
		I.waitForElement(this.emailConfirm.fieldErrors, 10)
	};

	//Наличие указанного сообщения об ошибке подтверждения почты
	seeErrorConfirm (value) {
		I.seeTextEquals(value, this.emailConfirm.fieldErrors);
	};

    //Наличие признака блокировки у кнопки повторной отправки кода подтверждения
	async seeDisabledResendCode () {
        I.assertEqual(await I.grabAttributeFrom(this.emailConfirm.buttonResendCode, 'disabled'), true)
    };

//Методы для страницы приветствия
    
    //Ожидание загрузки страницы приветствия
    waitWelcome () {
        I.waitInUrl(this.welcomePage.urlWelcome, 10);
    };

    //Наличие указанного текста в поле с текстом приветствия
    seeWelcome (value) {
        I.see(value, this.welcomePage.fieldTitlePage);
    };

    //Клик по кнопке создания связки
    clickCreateBundle () {
        I.click(this.welcomePage.buttonCreateBundle);
    };
};

module.exports = new SignupPage();