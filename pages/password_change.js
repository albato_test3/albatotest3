const { I } = inject();

class PasswordChangePage {
    
//Cтраница изменения пароля
    urlChangePassword = '/user/auth/reset-password'; //URL страницы
    formChangePassword = '.al-form'; //Форма сброса пароля
    logoAlbato = '.al-auth-modal-logo'; //Логотип Albato
    fieldTitleForm = '.al-auth-form-wrapper__titleh'; //Поле с названием формы
    textPassword = '//div[2]/div/input'; //Поле для ввода пароля
    textPasswordReplay = '//div[3]/div/input'; //Поле для ввода повторного пароля
    buttonHidePassword = '//div[2]/div/div/button'; //Кнопка видимости пароля
    buttonHidePasswordReplay = '//div[3]/div/div/button'; //Кнопка видимости повторного пароля
    buttonChange = '//div[4]/button'; //Кнопка изменения пароля
    pageLogin = '//div[2]/button/div'; //Кнопка страницы авторизации
    fieldErrorsPassword = '//div[2]/div/div[2]'; //Поле c ошибками ввода пароля
    fieldErrorsPasswordReplay = '//div[3]/div/div[2]'; //Поле c ошибками ввода повторного пароля

//Методы для страницы изменения пароля

    //Открытие страницы изменения пароля. Можно указать парметры c '&'
    openPage (params = '') {
        I.amOnPage(this.urlChangePassword + '?lang=ru' + params);
    };

    //Проверка текущего адреса страницы
    async seeCurrentPage (params = '') {
        I.assertContain(await I.grabCurrentUrl(), this.urlChangePassword + params);
    };

    //Ожидание загрузки формы изменения пароля
	waitFormChange () {
		I.waitForElement(this.formChangePassword, 10);
	};

    //Клик по логотипу Albato
	clickLogo () {
		I.click(this.logoAlbato);
	};

    //Наличие указанного названия формы. Для русского языка интерфейса название выставлено по умолчанию
    seeTitleForm (value = 'Введите новый пароль') {
        I.seeTextEquals (value, this.fieldTitleForm);
    };

    //Ввод пароля
	enterPassword (value) {
		I.fillField(this.textPassword, value)
	};

    //Ввод повторного пароля
	enterPasswordReplay (value) {
		I.fillField(this.textPasswordReplay, value)
	};

    //Клик по кнопке видимости пароля
    clickHidePassword () {
        I.click(this.buttonHidePassword);
    };

    //Клик по кнопке видимости повторного пароля
    clickHidePasswordReplay () {
        I.click(this.buttonHidePasswordReplay);
    };

    //Наличие указанного значения в поле пароля
    seeValueInPassword (value) {
        I.seeInField(this.textPassword, value);
    };

    //Наличие указанного значения в поле повторного пароля
    seeValueInPasswordReplay (value) {
        I.seeInField(this.textPasswordReplay, value);
    };

    //Клик по кнопке изменения пароля
	clickChange () {
		I.click(this.buttonChange);
	};

    //Наличие признака блокировки у кнопки изменения пароля
    async seeDisabledChange () {
        I.assertEqual(await I.grabAttributeFrom(this.buttonChange, 'disabled'), true)
    };

    //Ожидание загрузки кнопки страницы авторизации
    waitPageLogin () {
        I.waitForElement(this.pageLogin, 10);
    }

    //Клик по кнопке страницы авторизации
    clickPageLogin () {
        I.click(this.pageLogin);
    };

    //Наличие указанного сообщения об ошибке ввода пароля
	seeErrorPassword (value) {
		I.seeTextEquals(value, this.fieldErrorsPassword);
	};

    //Наличие указанного сообщения об ошибке ввода повторного пароля
	seeErrorPasswordReplay (value) {
		I.seeTextEquals(value, this.fieldErrorsPasswordReplay);
	};
}; 

module.exports = new PasswordChangePage();