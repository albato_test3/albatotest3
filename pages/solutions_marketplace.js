const { I } = inject();

class SolutionsMarketplace {

    //Локаторы

    urlSolutionsMarketplace = '/solutions/marketplace'; //URL страницы
    fieldHeader = {
        header: '.al-header', //Главный хедер
        menu: {
            dropDown: '.al-user-menu__control', //Кнопка выпадающего меню
            profile: '//li[1]', //Кнопка страницы профиля
            builderApp: '//li[2]', //Кнопка страницы конструктора приложений
            builderSolutions: '//li[3]', //Кнопка страницы конструктора решений
            oldAlbato: '//li[4]', //Кнопка страницы старого интерфейса
            signOut: '//li[5]' //Кнопка выхода из системы
        }
    };

    //Методы

    //Открытие страницы маркетплейса решений. Можно указать парметры c '&'
    openPage (params = '') {
        I.amOnPage(this.urlSolutionsMarketplace + params);
    };
    
    //Проверка текущего адреса страницы
    async seeCurrentPage (params = '') {
        I.assertContain(await I.grabCurrentUrl(), this.urlSolutionsMarketplace + params);
    };

    //Ожидание загрузки главного хедера
    waitHeader () {
        I.waitForElement(this.fieldHeader.header, 10);
    };
};

module.exports = new SolutionsMarketplace();