const { I } = inject();

class LoginPage {
    
//Cтраница авторизации
    urlLogin = '/user/auth/login'; //URL страницы
    formLogin = '.al-form'; //Форма авторизации
    logoAlbato = '.al-auth-modal-logo'; //Логотип Albato
    buttonLanguage = '//form/div[1]/div/div'; //Поле для выбора языка
    fieldTitleForm = '.al-auth-form-wrapper__titleh'; //Поле с названием формы
    textEmail = '[name="email"]'; //Поле для ввода почты
    textPassword = '[name="password"]'; //Поле для ввода пароля
    buttonLogin = '.al-button_variant_contained'; //Кнопка авторизации
    pagePasswordReset = '//div[6]/div/a'; //Кнопка страницы восстановления пароля
    pageRegistration = '//div[1]/div/div/a'; //Кнопка страницы регистрации
    fieldErrors = '.al-form-errors__text'; //Поле c ошибками авторизации
    buttonFacebook = '//div[2]/button[1]'; //Кнопка авторизации через Facebook
    buttonGoogle = '//div[2]/button[2]'; //Кнопка авторизации через Google
    
//Вкладка авторизации в Facebook
    tabFacebook = {
   		formLoginFacebook: '#loginform', //Форма авторизации в Facebook
    	textEmailFacebook: '[name="email"]', //Поле для ввода почты в Facebook
    	textPasswordFacebook: '[name="pass"]', //Поле для ввода пароля в Facebook
    	buttonLoginFacebook: '[name="login"]' //Кнопка авторизации в Facebook
    };
  
//Методы для страницы авторизации

	//Открытие страницы авторизации. Можно указать парметры c '&'
	openPage (params = '') {
   		I.amOnPage(this.urlLogin + '?lang=ru' + params);
	};

	//Проверка текущего адреса страницы
	async seeCurrentPage (params = '') {
		I.assertContain(await I.grabCurrentUrl(), this.urlLogin + params);
	};

	//Ожидание загрузки формы авторизации
	waitFormLogin () {
		I.waitForElement(this.formLogin, 10);
	};

	//Клик по логотипу Albato
	clickLogo () {
		I.click(this.logoAlbato);
	};

	//Выбор указанного языка
	selectLanguage (value) {
		I.click(this.buttonLanguage);
		I.click(`//*[text()="${value}"]`, '//ul')
	};

	//Наличие указанного названия формы. Для русского языка интерфейса название выставлено по умолчанию
	seeTitleForm (value = 'Войдите в аккаунт') {
		I.seeTextEquals(value, this.fieldTitleForm);
	};

	//Ввод почты
	enterEmail (value) {
		I.fillField(this.textEmail, value)
	};

	//Наличие признака обязательности у поля почты
	async seeWarningEmail () {
		I.assertContain(await I.grabAttributeFrom(this.textEmail, 'class'), 'border-errorBorder');
	};

	//Ввод пароля
	enterPassword (value) {
		I.fillField(this.textPassword, value);
	};

	//Наличие признака обязательности у поля пароля
	async seeWarningPassword () {
		I.assertContain(await I.grabAttributeFrom(this.textPassword, 'class'), 'border-errorBorder');
	};

	//Клик по кнопке авторизации
	clickLogin () {
		I.click(this.buttonLogin);
	};

	//Клик по кнопке страницы восстановления пароля
	clickPagePasswordReset () {
		I.click(this.pagePasswordReset);
	};

	//Клик по кнопке страницы регистрации
	clickPageRegistration () {
		I.click(this.pageRegistration);
	};

	//Ожидание загрузки поля с ошибками авторизации
	waitErrors () {
		I.waitForElement(this.fieldErrors, 10)
	};

	//Наличие указанного сообщения об ошибке
	seeError (value) {
		I.seeTextEquals(value, this.fieldErrors);
	};

	//Клик по кнопке авторизации через Facebook
	clickFacebook () {
		I.wait(5); //Клик без задержки не срабатывает
		I.click(this.buttonFacebook);
		I.wait(5); //Время между кликом и появлением вкладки Facebook
	};

	//Клик по кнопке авторизации через Google
	clickGoogle () {
		I.wait(5); //Клик без задержки не срабатывает
		I.click(this.buttonGoogle);
		I.wait(5); //Время между кликом и появлением вкладки Google
	};

	//Переключение на следующую вкладку (по умолчанию), либо на указанную
	switchTab (value) {
		I.switchToNextTab(value);
	};

//Методы для вкладки авторизации в Facebook

	//Заполнение формы авторизации в Facebook
	authFacebook (email, password) {
		I.waitForElement(this.tabFacebook.formLoginFacebook, 10);
		I.fillField(this.tabFacebook.textEmailFacebook, email);
		I.fillField(this.tabFacebook.textPasswordFacebook, password);
		I.forceClick(this.tabFacebook.buttonLoginFacebook);
		I.wait(10); //Без ожидания тесты будут падать
	};
};

//Экспортируем экземпляр класса
module.exports = new LoginPage();