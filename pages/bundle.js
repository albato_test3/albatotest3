const { I } = inject();

class BundlePage {

    //Локаторы
   
    urlBundle = '/bundle'; //URL страницы
    fieldHeader = {
        header: '.al-header', //Главный хедер
        menu: {
            dropDown: '.al-user-menu__control', //Кнопка выпадающего меню
            profile: '//li[1]', //Кнопка страницы профиля
            builderApp: '//li[2]', //Кнопка страницы конструктора приложений
            builderSolutions: '//li[3]', //Кнопка страницы конструктора решений
            oldAlbato: '//li[4]', //Кнопка страницы старого интерфейса
            signOut: '//li[6]' //Кнопка выхода из системы
        }
    };
    buttonCreateBundle = '.al-bundle-card-empty__button_create-bundle'; //Кнопка создания связки

    //Методы
    
    //Открытие страницы связок. Можно указать парметры c '&'
    openPage (params = '') {
    I.amOnPage(this.urlBundle + params);
    };
    
    //Проверка текущего адреса страницы
    async seeCurrentPage (params = '') {
        I.assertContain(await I.grabCurrentUrl(), this.urlBundle + params);
    };

    //Ожидание загрузки главного хедера
    waitHeader () {
        I.waitForElement(this.fieldHeader.header, 10);
    };

    //Ожидание загрузки выпадающего меню
    waitMenu () {
        I.waitForElement(this.fieldHeader.menu.dropDown, 10);
    };

    //Клик по указанной кнопке в выпадающем меню
    clickMenu (value) {
        I.click(this.fieldHeader.menu.dropDown);
        I.click(value);
    }

    //Ожидание загрузки кнопки создания связки
    waitbuttonCreateBundle () {
        I.waitForElement(this.buttonCreateBundle, 10);
    };
};

module.exports = new BundlePage();