const { I } = inject();

class PasswordResetPage {
    
//Cтраница сброса пароля
    urlResetPassword = '/user/auth/request-password-reset'; //URL страницы
    formResetPassword = '.al-form'; //Форма сброса пароля
    logoAlbato = '.al-auth-modal-logo'; //Логотип Albato
    buttonLanguage = '//form/div[1]/div/div'; //Поле для выбора языка
    fieldTitleForm = '.al-auth-form-wrapper__titleh'; //Поле с названием формы
    textEmail = '//div[2]/div/input'; //Поле для ввода почты
    buttonReset = '//button'; //Кнопка сброса пароля
    pageRegistration = '//div[4]/div/a[1]'; //Кнопка страницы регистрации
    pageLogin = '//div[4]/div/a[2]'; //Кнопка страницы авторизации
    fieldErrors = '.al-form-errors__text'; //Поле c ошибками сброса пароля

//Методы для страницы сброса пароля

    //Открытие страницы сброса пароля. Можно указать парметры c '&'
    openPage (params = '') {
        I.amOnPage(this.urlResetPassword + '?lang=ru' + params);
    };

    //Проверка текущего адреса страницы
    async seeCurrentPage (params = '') {
        I.assertContain(await I.grabCurrentUrl(), this.urlResetPassword + params);
    };

    //Ожидание загрузки формы сброса пароля
	waitFormReset () {
		I.waitForElement(this.formResetPassword, 10);
	};

    //Клик по логотипу Albato
	clickLogo () {
		I.click(this.logoAlbato);
	};

    //Выбор указанного языка
	selectLanguage (value) {
		I.click(this.buttonLanguage);
		I.click(`//*[text()="${value}"]`, '//ul')
	};

    //Наличие указанного названия формы. Для русского языка интерфейса название выставлено по умолчанию
    seeTitleForm (value = 'Введите email, который вы использовали при регистрации') {
        I.seeTextEquals (value, this.fieldTitleForm);
    };

    //Ввод почты
	enterEmail (value) {
		I.fillField(this.textEmail, value)
	};

    //Клик по кнопке сброса пароля
	clickReset () {
		I.click(this.buttonReset);
	};

    //Наличие признака блокировки у кнопки сброса пароля
    async seeDisabledReset () {
        I.assertEqual(await I.grabAttributeFrom(this.buttonReset, 'disabled'), true)
    };

    //Ожидание указанного текста на кнопке сброса пароля
    waitNameButton (value) {
        I.waitForText(value, 10, this.buttonReset);
    }

	//Клик по кнопке страницы регистрации
	clickPageRegistration () {
		I.click(this.pageRegistration);
	};

    //Клик по кнопке страницы авторизации
    clickPageLogin () {
        I.click(this.pageLogin);
    };

    //Ожидание загрузки поля с ошибками сброса пароля
	waitErrors () {
		I.waitForElement(this.fieldErrors, 10)
	};

    //Наличие указанного сообщения об ошибке
	seeError (value) {
		I.seeTextEquals(value, this.fieldErrors);
	};
}; 

module.exports = new PasswordResetPage();