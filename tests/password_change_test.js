Feature('Изменение пароля'); 

Scenario('@AP-118 - Создание нового пароля', async ({I, passwordChangePage, accountCreate, restorePassword, emailGeneration}) => {
    let account = await accountCreate.accountReg();
    await restorePassword.restorePassword(account.mail);
    I.wait(10);
    let link = await emailGeneration.getLinkPasswordReset();
    I.amOnPage(link);
    passwordChangePage.waitFormChange();
    passwordChangePage.enterPassword('tests123');
    passwordChangePage.enterPasswordReplay('tests123');
    passwordChangePage.clickChange();
    passwordChangePage.waitPageLogin();
    passwordChangePage.seeTitleForm('Пароль успешно изменен');
});

Scenario('@AP-58 - Переход на сайт кликом по логотипу Albato', async ({I, passwordChangePage}) => {
    passwordChangePage.openPage();
    passwordChangePage.waitFormChange();
    passwordChangePage.clickLogo();
    I.wait(2);
    I.switchToNextTab();
    I.seeCurrentUrlEquals('https://albato.ru/');
});

Scenario('@AP-131 - Переход на страницу авторизации после заполнения формы изменения пароля', async ({passwordChangePage, loginPage}) => {
    passwordChangePage.openPage('&token=')
    passwordChangePage.waitFormChange();
    passwordChangePage.enterPassword('123tests');
    passwordChangePage.enterPasswordReplay('123tests');
    passwordChangePage.clickChange();
    passwordChangePage.waitPageLogin();
    passwordChangePage.clickPageLogin();
    loginPage.seeCurrentPage();
});

Scenario('@AP-124 - Изменение видимости паролей', async ({passwordChangePage}) => {
    passwordChangePage.openPage();
    passwordChangePage.waitFormChange();
    passwordChangePage.enterPassword('123tests');
    passwordChangePage.clickHidePassword();
    passwordChangePage.seeValueInPassword('123tests');
    passwordChangePage.enterPasswordReplay('123tests');
    passwordChangePage.clickHidePasswordReplay();
    passwordChangePage.seeValueInPasswordReplay('123tests');
});

Scenario('@AP-125 - Пароль не заполнен', async ({passwordChangePage}) => {
    passwordChangePage.openPage();
    passwordChangePage.waitFormChange();
    passwordChangePage.enterPassword('');
    passwordChangePage.enterPasswordReplay('123');
    passwordChangePage.seeErrorPassword('Необходимо заполнить поле');
    await passwordChangePage.seeDisabledChange();
});

Scenario('@AP-127 - Повторный пароль не заполнен', async ({I, passwordChangePage}) => {
    passwordChangePage.openPage();
    passwordChangePage.waitFormChange();
    passwordChangePage.enterPassword('123');
    passwordChangePage.enterPasswordReplay('');
    I.click('body');
    passwordChangePage.seeErrorPasswordReplay('Пароли не совпадают');
    await passwordChangePage.seeDisabledChange();
});

Scenario('@AP-117 - Указаны разные пароли', async ({I, passwordChangePage}) => {
    passwordChangePage.openPage();
    passwordChangePage.waitFormChange();
    passwordChangePage.enterPassword('123');
    passwordChangePage.enterPasswordReplay('456');
    I.click('body');
    passwordChangePage.seeErrorPasswordReplay('Пароли не совпадают');
    await passwordChangePage.seeDisabledChange();
});

Scenario('@AP-121 - Указан пароль длиной меньше 6 символов', async ({I, passwordChangePage, accountCreate, restorePassword, emailGeneration}) => {
    let account = await accountCreate.accountReg();
    await restorePassword.restorePassword(account.mail);
    I.wait(10);
    let link = await emailGeneration.getLinkPasswordReset();
    I.amOnPage(link);
    passwordChangePage.waitFormChange();
    passwordChangePage.enterPassword('123');
    passwordChangePage.enterPasswordReplay('123');
    passwordChangePage.clickChange();
    passwordChangePage.waitPageLogin();
    passwordChangePage.seeTitleForm('Значение «Пароль» должно содержать минимум 6 символов.');
});

Scenario('@AP-116 - Указан пустой пароль', async ({I, passwordChangePage, accountCreate, restorePassword, emailGeneration}) => {
    let account = await accountCreate.accountReg();
    await restorePassword.restorePassword(account.mail);
    I.wait(10);
    let link = await emailGeneration.getLinkPasswordReset();
    I.amOnPage(link);
    passwordChangePage.waitFormChange();
    passwordChangePage.enterPassword(' ');
    passwordChangePage.enterPasswordReplay(' ');
    passwordChangePage.clickChange();
    passwordChangePage.waitPageLogin();
    passwordChangePage.seeTitleForm('Необходимо заполнить «Пароль».');
});

Scenario('@AP-128 - Указан текущий пароль', async ({I, passwordChangePage, accountCreate, restorePassword, emailGeneration}) => {
    let account = await accountCreate.accountReg();
    await restorePassword.restorePassword(account.mail);
    I.wait(10);
    let link = await emailGeneration.getLinkPasswordReset();
    I.amOnPage(link);
    passwordChangePage.waitFormChange();
    passwordChangePage.enterPassword('123tests');
    passwordChangePage.enterPasswordReplay('123tests');
    passwordChangePage.clickChange();
    passwordChangePage.waitPageLogin();
    passwordChangePage.seeTitleForm('Пароль успешно изменен');
});

Scenario('@AP-122 - Повторное изменение пароля по использованной ссылке', async ({I, passwordChangePage, accountCreate, restorePassword, emailGeneration}) => {
    let account = await accountCreate.accountReg();
    await restorePassword.restorePassword(account.mail);
    I.wait(10);
    let link = await emailGeneration.getLinkPasswordReset();
    for (let i = 1; i <= 2; i++) {
        I.amOnPage(link);
        passwordChangePage.waitFormChange();
        passwordChangePage.enterPassword('tests123');
        passwordChangePage.enterPasswordReplay('tests123');
        passwordChangePage.clickChange();
        passwordChangePage.waitPageLogin();
    };
    passwordChangePage.seeTitleForm('Неверный токен для сброса пароля');
});

Scenario('@AP-129 - Истек срок действия ссылки для изменения пароля', async ({passwordChangePage}) => {
    passwordChangePage.openPage('&token=999')
    passwordChangePage.waitFormChange();
    passwordChangePage.enterPassword('123tests');
    passwordChangePage.enterPasswordReplay('123tests');
    passwordChangePage.clickChange();
    passwordChangePage.waitPageLogin();
    passwordChangePage.seeTitleForm('Срок действия токена для сброса пароля истек');
});

Scenario('@AP-130 - Отсутствует токен в ссылке для изменения пароля', async ({passwordChangePage}) => {
    passwordChangePage.openPage('&token=')
    passwordChangePage.waitFormChange();
    passwordChangePage.enterPassword('123tests');
    passwordChangePage.enterPasswordReplay('123tests');
    passwordChangePage.clickChange();
    passwordChangePage.waitPageLogin();
    passwordChangePage.seeTitleForm('Необходимо заполнить «Token».');
});

Scenario('@AP-120 - Авторизация с новым паролем после его изменения', async ({I, passwordChangePage, accountCreate, restorePassword, emailGeneration, loginPage, bundlePage}) => {
    let account = await accountCreate.accountReg();
    await restorePassword.restorePassword(account.mail);
    I.wait(10);
    let link = await emailGeneration.getLinkPasswordReset();
    I.amOnPage(link);
    passwordChangePage.waitFormChange();
    passwordChangePage.enterPassword('tests123');
    passwordChangePage.enterPasswordReplay('tests123');
    passwordChangePage.clickChange();
    passwordChangePage.waitPageLogin();
    passwordChangePage.seeTitleForm('Пароль успешно изменен');
    loginPage.openPage();
    loginPage.waitFormLogin();
    loginPage.enterEmail(account.mail);
    loginPage.enterPassword('tests123');
    loginPage.clickLogin();
    bundlePage.waitHeader();
    await bundlePage.seeCurrentPage();
});

Scenario('@AP-119 - Авторизация со старым паролем после его изменения', async ({I, passwordChangePage, accountCreate, restorePassword, emailGeneration, loginPage}) => {
    let account = await accountCreate.accountReg();
    await restorePassword.restorePassword(account.mail);
    I.wait(10);
    let link = await emailGeneration.getLinkPasswordReset();
    I.amOnPage(link);
    passwordChangePage.waitFormChange();
    passwordChangePage.enterPassword('tests123');
    passwordChangePage.enterPasswordReplay('tests123');
    passwordChangePage.clickChange();
    passwordChangePage.waitPageLogin();
    passwordChangePage.seeTitleForm('Пароль успешно изменен');
    loginPage.openPage();
    loginPage.waitFormLogin();
    loginPage.enterEmail(account.mail);
    loginPage.enterPassword('123tests');
    loginPage.clickLogin();
    loginPage.waitErrors();
    loginPage.seeError('Неверный email или пароль');
});