Feature('Сброс пароля'); 

Scenario('@AP-51 - Указан действительный адрес почты', async ({passwordResetPage}) => {
    passwordResetPage.openPage();
    passwordResetPage.waitFormReset();
    passwordResetPage.enterEmail('autotests1albato@gmail.com');
    passwordResetPage.clickReset();
    passwordResetPage.waitNameButton('Отправить снова');
    passwordResetPage.seeTitleForm('Спасибо!\nВам отправлено письмо на email, который вы использовали при регистрации.');
});

Scenario('@AP-57 - Переход на сайт кликом по логотипу Albato', async ({I, passwordResetPage}) => {
    passwordResetPage.openPage();
    passwordResetPage.waitFormReset();
    passwordResetPage.clickLogo();
    I.wait(2)
    I.switchToNextTab();
    I.seeCurrentUrlEquals('https://albato.ru/');
});

Scenario('@AP-50 - Смена языков в форме сброса пароля', async ({I, passwordResetPage}) => {
    passwordResetPage.openPage();
    passwordResetPage.waitFormReset();
    passwordResetPage.selectLanguage('English');
    I.wait(1);
    passwordResetPage.seeTitleForm('Enter the email you used to sign up');
    passwordResetPage.selectLanguage('Русский');
    I.wait(1);
    passwordResetPage.seeTitleForm();
    passwordResetPage.selectLanguage('Português');
    I.wait(1);
    passwordResetPage.seeTitleForm('Confirme o endereço de E-mail que você usou no registro');
    passwordResetPage.selectLanguage('Español');
    I.wait(1);
    passwordResetPage.seeTitleForm('Introduzca el correo electrónico que utilizó para registrarse');
});

Scenario('@AP-48 - Переход на страницу авторизации', async ({passwordResetPage, loginPage}) => {
    passwordResetPage.openPage();
    passwordResetPage.waitFormReset();
    passwordResetPage.clickPageLogin();
    await loginPage.seeCurrentPage();
    loginPage.seeTitleForm();
});

Scenario('@AP-49 - Переход на страницу регистрации', async ({passwordResetPage, signupPage}) => {
    passwordResetPage.openPage();
    passwordResetPage.waitFormReset();
    passwordResetPage.clickPageRegistration();
    await signupPage.seeCurrentPage();
    signupPage.seeTitleForm();
});

Scenario('@AP-52 - Указан незарегистрированный адрес почты', async ({passwordResetPage}) => {
    passwordResetPage.openPage();
    passwordResetPage.waitFormReset();
    passwordResetPage.enterEmail('example@example.com');
    passwordResetPage.clickReset();
    passwordResetPage.waitErrors();
    passwordResetPage.seeError('Пользователь с указанным email адресом не существует');
});

Scenario('@AP-53 - Указан некорректный адрес почты', async ({passwordResetPage}) => {
    passwordResetPage.openPage();
    passwordResetPage.waitFormReset();
    passwordResetPage.enterEmail('example.com');
    passwordResetPage.clickReset();
    passwordResetPage.waitErrors();
    passwordResetPage.seeError('Значение «Email» не является правильным email адресом.');
});

Scenario('@AP-54 - Указан пустой адрес почты', async ({passwordResetPage}) => {
    passwordResetPage.openPage();
    passwordResetPage.waitFormReset();
    passwordResetPage.enterEmail(' ');
    passwordResetPage.clickReset();
    passwordResetPage.waitErrors();
    passwordResetPage.seeError('Необходимо заполнить «Email».');
});

Scenario('@AP-126 - Указан неподтвержденный адрес почты', async ({passwordResetPage}) => {
    passwordResetPage.openPage();
    passwordResetPage.waitFormReset();
    passwordResetPage.enterEmail('autotests4@albato.qa');
    passwordResetPage.clickReset();
    passwordResetPage.waitErrors();
    passwordResetPage.seeError('Email не подтвержден');
});

Scenario('@AP-123 - Поле адреса почты не заполнено', async ({passwordResetPage}) => {
    passwordResetPage.openPage();
    passwordResetPage.waitFormReset();
    await passwordResetPage.seeDisabledReset();
});

Scenario('@AP-55 - Получение письма со ссылкой на сброс пароля', async ({I, passwordResetPage, accountCreate, emailGeneration}) => {
    let account = await accountCreate.accountReg();
    passwordResetPage.openPage();
    passwordResetPage.waitFormReset();
    passwordResetPage.enterEmail(account.mail);
    passwordResetPage.clickReset();
    passwordResetPage.waitNameButton('Отправить снова');
    passwordResetPage.seeTitleForm('Спасибо!\nВам отправлено письмо на email, который вы использовали при регистрации.');
    I.wait(10);
    await emailGeneration.getLinkPasswordReset();
});

Scenario('@AP-56 - Повторная отправка письма', async ({passwordResetPage}) => {
    passwordResetPage.openPage();
    passwordResetPage.waitFormReset();
    passwordResetPage.enterEmail('autotests1albato@gmail.com');
    passwordResetPage.clickReset();
    passwordResetPage.waitNameButton('Отправить снова');
    passwordResetPage.clickReset();
    passwordResetPage.waitNameButton('Восстановить');
    passwordResetPage.seeTitleForm();
});