Feature('Регистрация');

Scenario('@AP-59 - Базовая регистрация', async ({signupPage, emailGeneration}) => {
    let name = `autotest${Date.now()}`
    let mail = await emailGeneration.getMailbox();
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(mail);
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.fillQuestionnaireUser();
    signupPage.clickSaveUser();
    signupPage.waitModalQuestionnaireServices();
    signupPage.seeTitleQuestionnaireServices();
    signupPage.clickSkipServices();
    signupPage.waitSnackbar();
    signupPage.seeSnackbar('Успешно сохранено')
    signupPage.waitFormConfirm();
    signupPage.seeTitleFormConfirm();
    signupPage.seeEmailUser(mail);
    let message = await emailGeneration.getCodeEmail();
    signupPage.enterCode(message);
    signupPage.waitWelcome();
    signupPage.seeWelcome(name);
});

Scenario('@AP-60 - Переход на сайт кликом по логотипу Albato', async ({I, signupPage}) => {
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.clickLogo();
    I.wait(2)
    I.switchToNextTab();
    I.seeCurrentUrlEquals('https://albato.ru/');
});

Scenario('@AP-61 - Смена языков формы', async ({I, signupPage}) => {
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.selectLanguage('English');
    I.wait(1);
    signupPage.seeTitleForm('Create an account');
    signupPage.selectLanguage('Русский');
    I.wait(1);
    signupPage.seeTitleForm();
    signupPage.selectLanguage('Português');
    I.wait(1);
    signupPage.seeTitleForm('Registrar uma conta nova');
    signupPage.selectLanguage('Español');
    I.wait(1);
    signupPage.seeTitleForm('Crear una cuenta');
});

Scenario('@AP-62 - Переход на страницу сброса пароля', async ({signupPage, passwordResetPage}) => {
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.clickPagePasswordReset();
    await passwordResetPage.seeCurrentPage();
    passwordResetPage.seeTitleForm();
});

Scenario('@AP-63 - Переход на страницу авторизации', async ({signupPage, loginPage}) => {
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.clickPageLogin();
    await loginPage.seeCurrentPage();
    loginPage.seeTitleForm();
});

Scenario('@AP-64 - Переход на страницу пользовательского соглашения', async ({I, signupPage}) => {
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.clickAgreement();
    I.wait(2)
    I.switchToNextTab();
    I.seeCurrentUrlEquals('https://albato.ru/offer');
});

Scenario('@AP-65 - Переход на страницу политики конфиденциальности', async ({I, signupPage}) => {
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.clickPolitics();
    I.wait(2)
    I.switchToNextTab();
    I.seeCurrentUrlEquals('https://albato.ru/confidential');
});

Scenario('@AP-66 - Поиск страны по названию в выпадающем списке', async ({signupPage}) => {
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterCountry('Brasil');
    signupPage.seeValueInCountryList('Brasil');
});

Scenario('@AP-67 - Сброс поиска страны по названию в выпадающем списке', async ({I, signupPage}) => {
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterCountry('Brasil');
    signupPage.seeValueInCountryList('Brasil');
    signupPage.clickClearCountry();
    I.seeElement('//li[2]');
});

Scenario('@AP-68 - Значение не найдено при поиске страны по названию в выпадающем списке', async ({I, signupPage}) => {
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterCountry('123');
    I.see('Значения отсутствуют');
});

Scenario('@AP-69 - Выбор страны телефона в выпадающем списке', async ({I, signupPage}) => {
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.selectFlagPhone('Brazil');
    I.wait(1);
    signupPage.seeValueInPhone('+55');
});

Scenario('@AP-70 - Изменение видимости пароля', async ({I, signupPage}) => {
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterPassword('123tests');
    signupPage.clickHidePassword();
    signupPage.seeValueInPassword('123tests');
});

Scenario('@AP-71 - Поле имени не заполнено', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitSnackbar();
    signupPage.seeSnackbar('Необходимо заполнить поле «Имя».');
});

Scenario('@AP-72 - Поле телефона не заполнено', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitSnackbar();
    signupPage.seeSnackbar('Необходимо заполнить поле «Телефон».');
});

Scenario('@AP-73 - Поле логина не заполнено', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitSnackbar();
    signupPage.seeSnackbar('Необходимо заполнить поле «Email».');
});

Scenario('@AP-74 - Поле пароля не заполнено', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.clickSignup();
    signupPage.waitSnackbar();
    signupPage.seeSnackbar('Необходимо заполнить поле «Пароль».');
});

Scenario('@AP-75 - Условия использования не приняты', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickConditions();
    signupPage.clickSignup();
    signupPage.waitSnackbar();
    signupPage.seeSnackbar('Необходимо принять условия Пользовательского соглашения и Политики конфиденциальности');
});

Scenario('@AP-76 - Указан неверный телефон', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('999');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitSnackbar();
    signupPage.seeSnackbar('Указан неверный номер телефона.');
});

Scenario('@AP-77 - Указан неверный адрес почты', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + 'test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitSnackbar();
    signupPage.seeSnackbar('Значение «Email» не является правильным email адресом.');
});

Scenario('@AP-78 - Указан пустой пароль', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword(' ');
    signupPage.clickSignup();
    signupPage.waitSnackbar();
    signupPage.seeSnackbar('Необходимо заполнить «Пароль».');
});

Scenario('@AP-79 - Указан зарегистрированный адрес почты', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail('autotests1@albato.qa');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitSnackbar();
    signupPage.seeSnackbar('Указанный email уже используется.');
});

Scenario('@AP-80 - Получение письма после заполнения формы регистрации', async ({signupPage, emailGeneration}) => {
    let name = `autotest${Date.now()}`
    let mail = await emailGeneration.getMailbox();
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(mail);
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    await emailGeneration.getCodeEmail();
});

Scenario('@AP-81 - Базовая регистрация с редиректом в маркетплейс решений', async ({signupPage, emailGeneration, solutionsMarketplacePage}) => {
    let name = `autotest${Date.now()}`
    let mail = await emailGeneration.getMailbox();
    signupPage.openPage(`&redirectTo=${solutionsMarketplacePage.urlSolutionsMarketplace}`);
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(mail);
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.fillQuestionnaireUser();
    signupPage.clickSaveUser();
    signupPage.waitModalQuestionnaireServices();
    signupPage.seeTitleQuestionnaireServices();
    signupPage.clickSkipServices();
    signupPage.waitSnackbar();
    signupPage.seeSnackbar('Успешно сохранено')
    signupPage.waitFormConfirm();
    signupPage.seeTitleFormConfirm();
    signupPage.seeEmailUser(mail);
    let message = await emailGeneration.getCodeEmail();
    signupPage.enterCode(message);
    solutionsMarketplacePage.waitHeader();
    await solutionsMarketplacePage.seeCurrentPage();
});

Scenario('@AP-82 - Базовая регистрация с редиректом в установку решения', async ({signupPage, emailGeneration, solutionsWizardPage}) => {
    let testSolutionId = '257'
    let name = `autotest${Date.now()}`
    let mail = await emailGeneration.getMailbox();
    signupPage.openPage(`&redirectTo=${solutionsWizardPage.urlSolutionsWizard}${testSolutionId}`);
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(mail);
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.fillQuestionnaireUser();
    signupPage.clickSaveUser();
    signupPage.waitModalQuestionnaireServices();
    signupPage.seeTitleQuestionnaireServices();
    signupPage.clickSkipServices();
    signupPage.waitSnackbar();
    signupPage.seeSnackbar('Успешно сохранено')
    signupPage.waitFormConfirm();
    signupPage.seeTitleFormConfirm();
    signupPage.seeEmailUser(mail);
    let message = await emailGeneration.getCodeEmail();
    signupPage.enterCode(message);
    solutionsWizardPage.waitHeader();
    await solutionsWizardPage.seeCurrentPage(testSolutionId);
    solutionsWizardPage.waitScenarios();
    solutionsWizardPage.seeNameSolution('For AutoTests');
});

Scenario('@AP-83 - Базовая регистрация с редиректом создания связки из виджета сайта', async ({I, signupPage, emailGeneration, bundlePage, bundleConstructorPage}) => {
    let testWidgetUrl = '%2Fbundle%3Fcreate%3Dtrue%26sourceId%3D38%26triggerId%3D38001%26targetId%3D21%26actionId%3D21006'
    let name = `autotest${Date.now()}`
    let mail = await emailGeneration.getMailbox();
    signupPage.openPage(`&redirectTo=${testWidgetUrl}`);
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(mail);
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.fillQuestionnaireUser();
    signupPage.clickSaveUser();
    signupPage.waitModalQuestionnaireServices();
    signupPage.seeTitleQuestionnaireServices();
    signupPage.clickSkipServices();
    signupPage.waitSnackbar();
    signupPage.seeSnackbar('Успешно сохранено')
    signupPage.waitFormConfirm();
    signupPage.seeTitleFormConfirm();
    signupPage.seeEmailUser(mail);
    let message = await emailGeneration.getCodeEmail();
    signupPage.enterCode(message);
    bundleConstructorPage.waitHeader();
    bundleConstructorPage.waitStepsBundle();
    I.assertContain(await I.grabCurrentUrl(), `${bundlePage.urlBundle}/create/`);
    bundleConstructorPage.seeTextInStepsBundle('Шаг 1 - Вебхук');
    bundleConstructorPage.seeTextInStepsBundle('Шаг 2 - Google Sheets');
});

Scenario('@AP-94 - Сохранение незаполненной анкеты “О себе”', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    await signupPage.seeDisabledSaveUser();
});

Scenario('@AP-95 - Поиск значений выпадающего списка анкеты “О себе”', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.enterAnswer('Недвижимость');
    signupPage.seeValueInAnswerList('Недвижимость');
});

Scenario('@AP-96 - Сброс поиска значений выпадающего списка анкеты “О себе”', async ({I, signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.enterAnswer('Недвижимость');
    signupPage.seeValueInAnswerList('Недвижимость');
    signupPage.clickClearAnswer();
    I.seeElement('//li[2]');
});

Scenario('@AP-97 - Поиск значений выпадающего списка анкеты “О себе”', async ({I, signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.enterAnswer('123');
    I.see('Значения отсутствуют');
});

Scenario('@AP-98 - Сохранение заполненной анкеты “Сервисы”', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.fillQuestionnaireUser();
    signupPage.clickSaveUser();
    signupPage.waitModalQuestionnaireServices();
    signupPage.seeTitleQuestionnaireServices();
    signupPage.waitServices('Calltouch');
    signupPage.selectServices(10);
    signupPage.clickSaveServices();
    signupPage.waitSnackbar();
    signupPage.seeSnackbar('Успешно сохранено')
    signupPage.waitFormConfirm();
    signupPage.seeTitleFormConfirm();
});

Scenario('@AP-99 - Сохранение незаполненной анкеты “Сервисы”', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.fillQuestionnaireUser();
    signupPage.clickSaveUser();
    signupPage.waitModalQuestionnaireServices();
    signupPage.seeTitleQuestionnaireServices();
    signupPage.waitServices('Calltouch');
    await signupPage.seeDisabledSaveServices();
});

Scenario('@AP-100 - Пропуск заполненной анкеты “Сервисы”', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.fillQuestionnaireUser();
    signupPage.clickSaveUser();
    signupPage.waitModalQuestionnaireServices();
    signupPage.seeTitleQuestionnaireServices();
    signupPage.waitServices('Calltouch');
    signupPage.selectServices(10);
    await signupPage.seeDisabledSkip();
});

Scenario('@AP-101 - Поиск сервиса по названию в анкете “Сервисы”', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.fillQuestionnaireUser();
    signupPage.clickSaveUser();
    signupPage.waitModalQuestionnaireServices();
    signupPage.seeTitleQuestionnaireServices();
    signupPage.waitServices('Calltouch');
    signupPage.enterServices('Mailchimp');
    signupPage.waitServices('MailChimp');
});

Scenario('@AP-102 - Сброс поиска сервиса по названию в анкете “Сервисы”', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.fillQuestionnaireUser();
    signupPage.clickSaveUser();
    signupPage.waitModalQuestionnaireServices();
    signupPage.seeTitleQuestionnaireServices();
    signupPage.waitServices('Calltouch');
    signupPage.enterServices('Mailchimp');
    signupPage.waitServices('MailChimp');
    signupPage.clearServices();
    signupPage.waitServices('Calltouch');
});

Scenario('@AP-103 - Значение не найдено при поиске сервиса по названию в анкете “Сервисы”', async ({I, signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.fillQuestionnaireUser();
    signupPage.clickSaveUser();
    signupPage.waitModalQuestionnaireServices();
    signupPage.seeTitleQuestionnaireServices();
    signupPage.waitServices('Calltouch');
    signupPage.enterServices('123');
    I.waitToHide(signupPage.questionnaireServices.fieldServicesList + '/div[1]', 10);
    signupPage.seeTitleQuestionnaireServices();
});

Scenario('@AP-104 - Поиск сервиса по скроллу в анкете “Сервисы”', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.fillQuestionnaireUser();
    signupPage.clickSaveUser();
    signupPage.waitModalQuestionnaireServices();
    signupPage.seeTitleQuestionnaireServices();
    signupPage.waitServices('Calltouch');
    signupPage.scrollServices(36);
    signupPage.waitServices('MailChimp');
});

Scenario('@AP-105 - Выбор более 10 сервисов в анкете “Сервисы”', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.fillQuestionnaireUser();
    signupPage.clickSaveUser();
    signupPage.waitModalQuestionnaireServices();
    signupPage.seeTitleQuestionnaireServices();
    signupPage.waitServices('Calltouch');
    signupPage.selectServices(20);
    signupPage.seeCounter('10/10')
});

Scenario('@AP-106 - Удаление выбранного сервиса кликом по сервису в анкете “Сервисы”', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.fillQuestionnaireUser();
    signupPage.clickSaveUser();
    signupPage.waitModalQuestionnaireServices();
    signupPage.seeTitleQuestionnaireServices();
    signupPage.waitServices('Calltouch');
    signupPage.selectServices(10);
    signupPage.seeCounter('10/10');
    signupPage.selectServices(5);
    signupPage.seeCounter('5/10');
});

Scenario('@AP-107 - Удаление выбранного сервиса кликом по тегу в анкете “Сервисы”', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.fillQuestionnaireUser();
    signupPage.clickSaveUser();
    signupPage.waitModalQuestionnaireServices();
    signupPage.seeTitleQuestionnaireServices();
    signupPage.waitServices('Calltouch');
    signupPage.selectServices(10);
    signupPage.seeCounter('10/10');
    signupPage.clickTags(5);
    signupPage.seeCounter('5/10');
});

Scenario('@AP-108 - Переход на сайт кликом по логотипу Albato в форме подтверждения почты', async ({I, signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.fillQuestionnaireUser();
    signupPage.clickSaveUser();
    signupPage.waitModalQuestionnaireServices();
    signupPage.seeTitleQuestionnaireServices();
    signupPage.clickSkipServices();
    signupPage.waitSnackbar();
    signupPage.seeSnackbar('Успешно сохранено')
    signupPage.waitFormConfirm();
    signupPage.seeTitleFormConfirm();
    signupPage.clickLogoConfirm();
    I.wait(2);
    I.switchToNextTab();
    I.seeCurrentUrlEquals('https://albato.ru/');
});

Scenario('@AP-109 - Смена языков формы подтверждения почты', async ({I, signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.fillQuestionnaireUser();
    signupPage.clickSaveUser();
    signupPage.waitModalQuestionnaireServices();
    signupPage.seeTitleQuestionnaireServices();
    signupPage.clickSkipServices();
    signupPage.waitSnackbar();
    signupPage.seeSnackbar('Успешно сохранено')
    signupPage.waitFormConfirm();
    signupPage.selectLanguageConfirm('English');
    I.wait(1);
    signupPage.seeTitleFormConfirm('Check your email');
    signupPage.seeEmailUser(name + '@test.com');
    signupPage.selectLanguageConfirm('Русский');
    I.wait(1);
    signupPage.seeTitleFormConfirm();
    signupPage.seeEmailUser(name + '@test.com');
    signupPage.selectLanguageConfirm('Português');
    I.wait(1);
    signupPage.seeTitleFormConfirm('Verifique seu e-mail');
    signupPage.seeEmailUser(name + '@test.com');
    signupPage.selectLanguageConfirm('Español');
    I.wait(1);
    signupPage.seeTitleFormConfirm('Revise su correo electrónico');
    signupPage.seeEmailUser(name + '@test.com');
});

Scenario('@AP-110 - Подтверждение почты после повторной отправки кода подтверждения', async ({I, signupPage, emailGeneration}) => {
    let name = `autotest${Date.now()}`
    let mail = await emailGeneration.getMailbox();
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(mail);
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.fillQuestionnaireUser();
    signupPage.clickSaveUser();
    signupPage.waitModalQuestionnaireServices();
    signupPage.seeTitleQuestionnaireServices();
    signupPage.clickSkipServices();
    signupPage.waitSnackbar();
    signupPage.seeSnackbar('Успешно сохранено')
    signupPage.waitFormConfirm();
    signupPage.seeTitleFormConfirm();
    signupPage.seeEmailUser(mail);
    signupPage.waitTimerExpired();
    signupPage.clickResend();
    I.wait(10);
    let message = await emailGeneration.getCodeEmailResend();
    signupPage.enterCode(message);
    signupPage.waitWelcome();
    signupPage.seeWelcome(name);
});

Scenario('@AP-111 - Повторная отправка кода подтверждения почты до истечения таймера', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.fillQuestionnaireUser();
    signupPage.clickSaveUser();
    signupPage.waitModalQuestionnaireServices();
    signupPage.seeTitleQuestionnaireServices();
    signupPage.clickSkipServices();
    signupPage.waitSnackbar();
    signupPage.seeSnackbar('Успешно сохранено')
    signupPage.waitFormConfirm();
    signupPage.seeTitleFormConfirm();
    await signupPage.seeDisabledResendCode();
});

Scenario('@AP-112 - Подтверждение почты после авторизации в неподтвержденный аккаунт', async ({I, loginPage, signupPage, emailGeneration, accountCreate}) => {
    let account = await accountCreate.accountRegNotConfirmed();
    loginPage.openPage();
    loginPage.waitFormLogin();
    loginPage.enterEmail(account.mail);
    loginPage.enterPassword(account.password);
    loginPage.clickLogin();
    loginPage.waitErrors();
    loginPage.clickLogin();
    signupPage.waitFormConfirm();
    signupPage.seeTitleFormConfirm();
    signupPage.seeEmailUser(account.mail);
    I.wait(10);
    let message = await emailGeneration.getCodeEmailResend();
    signupPage.enterCode(message);
    signupPage.waitWelcome();
    signupPage.seeWelcome(account.name);
});

Scenario('@AP-113 - Указан неверный код подтверждения почты', async ({signupPage}) => {
    let name = `autotest${Date.now()}`
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(name + '@test.com');
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.fillQuestionnaireUser();
    signupPage.clickSaveUser();
    signupPage.waitModalQuestionnaireServices();
    signupPage.seeTitleQuestionnaireServices();
    signupPage.clickSkipServices();
    signupPage.waitSnackbar();
    signupPage.seeSnackbar('Успешно сохранено')
    signupPage.waitFormConfirm();
    signupPage.seeTitleFormConfirm();
    signupPage.enterCode('12345678');
    signupPage.waitErrorsConfirm();
    signupPage.seeErrorConfirm('Недействительный код подтверждения адреса электронной почты');
});

Scenario('@AP-114 - Указан предыдущий отправленный код подтверждения почты', async ({I, signupPage, emailGeneration}) => {
    let name = `autotest${Date.now()}`
    let mail = await emailGeneration.getMailbox();
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(mail);
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.fillQuestionnaireUser();
    signupPage.clickSaveUser();
    signupPage.waitModalQuestionnaireServices();
    signupPage.seeTitleQuestionnaireServices();
    signupPage.clickSkipServices();
    signupPage.waitSnackbar();
    signupPage.seeSnackbar('Успешно сохранено')
    signupPage.waitFormConfirm();
    signupPage.seeTitleFormConfirm();
    signupPage.seeEmailUser(mail);
    let message = await emailGeneration.getCodeEmail();
    signupPage.waitTimerExpired();
    signupPage.clickResend();
    I.wait(2);
    signupPage.enterCode(message);
    signupPage.waitErrorsConfirm();
    signupPage.seeErrorConfirm('Недействительный код подтверждения адреса электронной почты');
});

Scenario('@AP-115 - Переход в конструктор связки на странице приветствия после подтверждения почты', async ({I, signupPage, emailGeneration, bundlePage, bundleConstructorPage}) => {
    let name = `autotest${Date.now()}`
    let mail = await emailGeneration.getMailbox();
    signupPage.openPage();
    signupPage.waitFormSignup();
    signupPage.enterName(name);
    signupPage.selectCountry('Россия');
    signupPage.enterPhone('9998887766');
    signupPage.enterEmail(mail);
    signupPage.enterPassword('123tests');
    signupPage.clickSignup();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.fillQuestionnaireUser();
    signupPage.clickSaveUser();
    signupPage.waitModalQuestionnaireServices();
    signupPage.seeTitleQuestionnaireServices();
    signupPage.clickSkipServices();
    signupPage.waitSnackbar();
    signupPage.seeSnackbar('Успешно сохранено')
    signupPage.waitFormConfirm();
    signupPage.seeTitleFormConfirm();
    signupPage.seeEmailUser(mail);
    let message = await emailGeneration.getCodeEmail();
    signupPage.enterCode(message);
    signupPage.waitWelcome();
    signupPage.seeWelcome(name);
    signupPage.clickCreateBundle();
    bundleConstructorPage.waitHeader();
    bundleConstructorPage.waitStepsBundle();
    I.assertContain(await I.grabCurrentUrl(), `${bundlePage.urlBundle}/create/`);
    bundleConstructorPage.seeTextInStepsBundle('Добавьте триггер, который будет запускать связку');
    bundleConstructorPage.seeTextInStepsBundle('Добавьте действие, которое будет происходить после старта связки');
});
