Feature('Авторизация');

Scenario('@AP-20 - Базовая авторизация', async ({loginPage, bundlePage}) => {
    loginPage.openPage();
    loginPage.waitFormLogin();
    loginPage.enterEmail('autotests1@albato.qa');
    loginPage.enterPassword('123tests');
    loginPage.clickLogin();
    bundlePage.waitHeader();
    await bundlePage.seeCurrentPage();
});

Scenario('@AP-24 - Переход на сайт кликом по логотипу Albato', async ({I, loginPage}) => {
    loginPage.openPage();
    loginPage.waitFormLogin();
    loginPage.clickLogo();
    I.wait(2)
    I.switchToNextTab();
    I.seeCurrentUrlEquals('https://albato.ru/');
});

Scenario('@AP-25 - Смена языков формы', async ({I, loginPage}) => {
    loginPage.openPage();
    loginPage.waitFormLogin();
    loginPage.selectLanguage('English');
    I.wait(1);
    loginPage.seeTitleForm('Sign in to your account');
    loginPage.selectLanguage('Русский');
    I.wait(1);
    loginPage.seeTitleForm();
    loginPage.selectLanguage('Português');
    I.wait(1);
    loginPage.seeTitleForm('Acesse sua conta');
    loginPage.selectLanguage('Español');
    I.wait(1);
    loginPage.seeTitleForm('Iniciar sesión');
});

Scenario('@AP-26 - Переход на страницу сброса пароля', async ({loginPage, passwordResetPage}) => {
    loginPage.openPage();
    loginPage.waitFormLogin();
    loginPage.clickPagePasswordReset();
    await passwordResetPage.seeCurrentPage();
    passwordResetPage.seeTitleForm();
});

Scenario('@AP-27 - Переход на страницу регистрации', async ({loginPage, signupPage}) => {
    loginPage.openPage();
    loginPage.waitFormLogin();
    loginPage.clickPageRegistration();
    await signupPage.seeCurrentPage();
    signupPage.seeTitleForm();
});

Scenario('@AP-28 - Поле логина не заполнено', async ({loginPage}) => {
    loginPage.openPage();
    loginPage.waitFormLogin();
    loginPage.enterPassword('123tests');
    loginPage.clickLogin();
    await loginPage.seeWarningEmail();
});

Scenario('@AP-29 - Поле пароля не заполнено', async ({loginPage}) => {
    loginPage.openPage();
    loginPage.waitFormLogin();
    loginPage.enterEmail('autotests1@albato.qa');
    loginPage.clickLogin();
    await loginPage.seeWarningPassword();
});

Scenario('@AP-30 - Указан незарегистрированный адрес почты', async ({loginPage}) => {
    loginPage.openPage();
    loginPage.waitFormLogin();
    loginPage.enterEmail('example@example.com');
    loginPage.enterPassword('123tests');
    loginPage.clickLogin();
    loginPage.waitErrors();
    loginPage.seeError('Неверный email или пароль');
});

Scenario('@AP-31 - Указан неверный пароль', async ({loginPage}) => {
    loginPage.openPage();
    loginPage.waitFormLogin();
    loginPage.enterEmail('autotests1@albato.qa');
    loginPage.enterPassword('x123tests');
    loginPage.clickLogin();
    loginPage.waitErrors();
    loginPage.seeError('Неверный email или пароль');
});

Scenario('@AP-46 - Указан пустой пароль', async ({loginPage}) => {
    loginPage.openPage();
    loginPage.waitFormLogin();
    loginPage.enterEmail('autotests1@albato.qa');
    loginPage.enterPassword(' ');
    loginPage.clickLogin();
    loginPage.waitErrors();
    loginPage.seeError('Необходимо заполнить «Пароль».');
});

Scenario('@AP-32 - Авторизация в неподтвержденный аккаунт', async ({I, loginPage}) => {
    loginPage.openPage();
    loginPage.waitFormLogin();
    loginPage.enterEmail('autotests2@albato.qa');
    loginPage.enterPassword('123tests');
    loginPage.clickLogin();
    loginPage.waitErrors();
    loginPage.seeError('Email не подтвержден');
    I.see('Подтвердить Email', loginPage.buttonLogin);
});

Scenario('@AP-45 - Выход из системы', async ({loginPage, bundlePage}) => {
    loginPage.openPage();
    loginPage.waitFormLogin();
    loginPage.enterEmail('autotests1@albato.qa');
    loginPage.enterPassword('123tests');
    loginPage.clickLogin();
    bundlePage.waitHeader();
    await bundlePage.seeCurrentPage();
    bundlePage.waitMenu();
    bundlePage.clickMenu(bundlePage.fieldHeader.menu.signOut);
    loginPage.waitFormLogin();
    await loginPage.seeCurrentPage();
    loginPage.seeTitleForm();
});

Scenario('@AP-21 - Базовая авторизация с редиректом в маркетплейс решений', async ({loginPage, solutionsMarketplacePage}) => {
    loginPage.openPage(`&redirectTo=${solutionsMarketplacePage.urlSolutionsMarketplace}`);
    loginPage.waitFormLogin();
    loginPage.enterEmail('autotests1@albato.qa');
    loginPage.enterPassword('123tests');
    loginPage.clickLogin();
    solutionsMarketplacePage.waitHeader();
    await solutionsMarketplacePage.seeCurrentPage();
});

Scenario('@AP-22 - Базовая авторизация с редиректом в установку решения', async ({loginPage, solutionsWizardPage}) => {
    let testSolutionId = '257'
    loginPage.openPage(`&redirectTo=${solutionsWizardPage.urlSolutionsWizard}${testSolutionId}`);
    loginPage.waitFormLogin();
    loginPage.enterEmail('autotests1@albato.qa');
    loginPage.enterPassword('123tests');
    loginPage.clickLogin();
    solutionsWizardPage.waitHeader();
    await solutionsWizardPage.seeCurrentPage(testSolutionId);
    solutionsWizardPage.waitScenarios();
    solutionsWizardPage.seeNameSolution('For AutoTests');
});

Scenario('@AP-23 - Базовая авторизация с редиректом создания связки из виджета сайта', async ({I, loginPage, bundlePage, bundleConstructorPage}) => {
    let testWidgetUrl = '%2Fbundle%3Fcreate%3Dtrue%26sourceId%3D38%26triggerId%3D38001%26targetId%3D21%26actionId%3D21006'
    loginPage.openPage(`&redirectTo=${testWidgetUrl}`);
    loginPage.waitFormLogin();
    loginPage.enterEmail('autotests1@albato.qa');
    loginPage.enterPassword('123tests');
    loginPage.clickLogin();
    bundleConstructorPage.waitHeader();
    bundleConstructorPage.waitStepsBundle();
    I.assertContain(await I.grabCurrentUrl(), `${bundlePage.urlBundle}/create/`);
    bundleConstructorPage.seeTextInStepsBundle('Шаг 1 - Вебхук');
    bundleConstructorPage.seeTextInStepsBundle('Шаг 2 - Google Sheets');
});

// Scenario('@AP-33 - Авторизация через Facebook', async ({loginPage, bundlePage}) => {
//     loginPage.openPage();
//     loginPage.waitFormLogin();
//     loginPage.clickFacebook();
//     loginPage.switchTab();
//     loginPage.authFacebook('');
//     loginPage.switchTab(1);
//     bundlePage.waitHeader();
//     await bundlePage.seeCurrentPage();
// });

// Scenario('@AP-34 - Авторизация через Facebook с редиректом в маркетплейс решений', async ({loginPage, solutionsMarketplacePage}) => {
//     loginPage.openPage(`&redirectTo=${solutionsMarketplacePage.urlSolutionsMarketplace}`);
//     loginPage.waitFormLogin();
//     loginPage.clickFacebook();
//     loginPage.switchTab();
//     loginPage.authFacebook('');
//     loginPage.switchTab(1);
//     solutionsMarketplacePage.waitHeader();
//     await solutionsMarketplacePage.seeCurrentPage();
// });

// Scenario('@AP-35 - Авторизация через Facebook с редиректом в установку решения', async ({loginPage, solutionsWizardPage}) => {
//     let testSolutionId = '257'
//     loginPage.openPage(`&redirectTo=${solutionsWizardPage.urlSolutionsWizard}${testSolutionId}`);
//     loginPage.waitFormLogin();
//     loginPage.clickFacebook();
//     loginPage.switchTab();
//     loginPage.authFacebook('');
//     loginPage.switchTab(1);
//     solutionsWizardPage.waitHeader();
//     await solutionsWizardPage.seeCurrentPage(testSolutionId);
//     solutionsWizardPage.waitScenarios();
//     solutionsWizardPage.seeNameSolution('For AutoTests');
// });

// Scenario('@AP-36 - Авторизация через Facebook с редиректом создания связки из виджета сайта', async ({I, loginPage, bundlePage, bundleConstructorPage}) => {
//     let testWidgetUrl = '%2Fbundle%3Fcreate%3Dtrue%26sourceId%3D38%26triggerId%3D38001%26targetId%3D21%26actionId%3D21006'
//     loginPage.openPage(`&redirectTo=${testWidgetUrl}`);
//     loginPage.waitFormLogin();
//     loginPage.clickFacebook();
//     loginPage.switchTab();
//     loginPage.authFacebook('');
//     loginPage.switchTab(1);
//     bundleConstructorPage.waitHeader();
//     bundleConstructorPage.waitStepsBundle();
//     I.assertContain(await I.grabCurrentUrl(), `${bundlePage.urlBundle}/create/`);
//     bundleConstructorPage.seeTextInStepsBundle('Шаг 1 - Вебхук');
//     bundleConstructorPage.seeTextInStepsBundle('Шаг 2 - Google Sheets');
// });

// Scenario('@AP-37 - Авторизация через Facebook в незарегистрированный аккаунт', async ({loginPage, signupPage}) => {
//     loginPage.openPage();
//     loginPage.waitFormLogin();
//     loginPage.clickFacebook();
//     loginPage.switchTab();
//     loginPage.authFacebook('');
//     loginPage.switchTab(1);
//     await signupPage.seeCurrentPage();
//     signupPage.seeTitleForm();
// });

// Scenario('@AP-38 - Авторизация через Facebook в незарегистрированный аккаунт с редиректом создания связки из виджета сайта', async ({loginPage, signupPage}) => {
//     let testWidgetUrl = '%2Fbundle%3Fcreate%3Dtrue%26sourceId%3D38%26triggerId%3D38001%26targetId%3D21%26actionId%3D21006'
//     loginPage.openPage(`&redirectTo=${testWidgetUrl}`);
//     loginPage.waitFormLogin();
//     loginPage.clickFacebook();
//     loginPage.switchTab();
//     loginPage.authFacebook('');
//     loginPage.switchTab(1);
//     await signupPage.seeCurrentPage();
//     signupPage.seeTitleForm();
// });

Scenario('@AP-47 - Пропуск анкеты "О себе" после авторизации', async ({resetQuestionnaireUser, loginPage, bundlePage, signupPage}) => {
    await resetQuestionnaireUser.resetQuestionnaireUser('autotests3@albato.qa', '123tests');
    loginPage.openPage();
    loginPage.waitFormLogin();
    loginPage.enterEmail('autotests3@albato.qa');
    loginPage.enterPassword('123tests');
    loginPage.clickLogin();
    signupPage.waitModalQuestionnaireUser();
    signupPage.seeTitleQuestionnaireUser();
    signupPage.clickSkipUser();
    bundlePage.waitHeader();
    await bundlePage.seeCurrentPage();
});