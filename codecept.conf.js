exports.config = {
  tests: "./tests/*_test.js",
  output: "./output",
  helpers: {
    Playwright: {
      url: "https://albato-staging.com/app",
      show: false,
      browser: "chromium"
    },
    REST: {},
    JSONResponse: {},
    ChaiWrapper: {
      require: "codeceptjs-chai"
    },
    MailSlurp: {
      require: '@codeceptjs/mailslurp-helper',
      apiKey: 'cc54b11fbef4ebfe5f724ae6e9e8311f684880fcf06754c67cab091ed7c99220'
    },

  },
  include: {
    emailGeneration: "./modules/email_generation.js",
    accountCreate: "./modules/account_create.js",
    restorePassword: "./modules/restore_password.js",
    resetQuestionnaireUser: "./modules/reset_questionnaire_user.js",
    loginPage: "./pages/login.js",
    bundlePage: "./pages/bundle.js",
    bundleConstructorPage: "./pages/bundle_constructor.js",
    passwordResetPage: "./pages/password_reset.js",
    passwordChangePage: "./pages/password_change.js",
    signupPage: "./pages/signup.js",
    solutionsMarketplacePage: "./pages/solutions_marketplace.js",
    solutionsWizardPage: "./pages/solutions_wizard.js"
  },
  bootstrap: null,
  mocha: {
    "reporterOptions": {
        "reportDir": "output"
    }
  },
  name: "my test project",
}; 